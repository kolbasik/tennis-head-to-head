﻿using System.Data.Entity;

namespace Simple.Mvc4.Security
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : this(Contracts.SecurityConnection)
        {
            
        }

        public UsersContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        public DbSet<UserModel> Users { get; set; }
    }
}
