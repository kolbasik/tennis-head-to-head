namespace Simple.Mvc4.Security
{
    internal static class Contracts
    {
        public const string SecurityConnection = "SecurityConnection";
        public const string LogsConnection = "elmah-sqlserver";
        
        public static class Users 
        {
            public const string TableName = "Users";
            public const string UserId = "UserId";
            public const string UserName = "UserName";
        }
    }

    public static class SecurityContracts
    {
        public static class User
        {
            public const string Admin = "Mario";
        }

        public static class Role
        {
            public const string Admin = "Admin";
        }
    }
}