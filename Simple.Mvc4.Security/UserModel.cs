using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Simple.Mvc4.Security
{
    [Table(Contracts.Users.TableName)]
    public class UserModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column(Contracts.Users.UserId)]
        public int UserId { get; set; }

        [Column(Contracts.Users.UserName)]
        public string UserName { get; set; }
    }
}