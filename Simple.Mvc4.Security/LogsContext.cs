using System.Data.Entity;

namespace Simple.Mvc4.Security
{
    public class LogsContext : DbContext
    {
        public LogsContext()
            : this(Contracts.LogsConnection)
        {
        }

        public LogsContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }
    }
}