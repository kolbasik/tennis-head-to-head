using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Web.Security;
using WebMatrix.WebData;

namespace Simple.Mvc4.Security
{
    public static class SimpleMembershipConfig
    {
        public static void RegisterSimpleMembership()
        {
            Database.SetInitializer<UsersContext>(null);
            Database.SetInitializer<LogsContext>(null);

            try
            {
                CreateDatabaseIfNotExists<UsersContext>();
                CreateDatabaseIfNotExists<LogsContext>();

                WebSecurity.InitializeDatabaseConnection(Contracts.SecurityConnection, Contracts.Users.TableName, Contracts.Users.UserId, Contracts.Users.UserName, autoCreateTables: true);

                EnsurePrimaryUsers();
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588", ex);
            }
        }

        private static void CreateDatabaseIfNotExists<TContext>()
            where TContext : DbContext, new()
        {

            using (var context = new TContext())
            {
                if (!context.Database.Exists())
                {
                    // Create the database without Entity Framework migration schema
                    ((IObjectContextAdapter)context).ObjectContext.CreateDatabase();
                }
            }
        }

        private static void EnsurePrimaryUsers()
        {
            if (!Roles.RoleExists(SecurityContracts.Role.Admin))
            {
                Roles.CreateRole(SecurityContracts.Role.Admin);
            }
            if (!WebSecurity.UserExists(SecurityContracts.User.Admin))
            {
                WebSecurity.CreateUserAndAccount(SecurityContracts.User.Admin, "1qazxsw2");
                Roles.AddUserToRole(SecurityContracts.User.Admin, SecurityContracts.Role.Admin);
            }
        }
    }
}