using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
//using System.Data.Services.Common;
using Simple.Core.Data;

namespace Simple.Data.Models
{
    [Table("location_Countries")]
    [ScaffoldTable(true)]
    //[DataServiceKey("Id")]
    public class CountryModel : IDataEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public string Name { get; set; }

        [ForeignKey("CountryId")]
        public virtual ICollection<RegionModel> Regions { get; set; }
    }

    [Table("location_Regions")]
    [ScaffoldTable(true)]
    //[DataServiceKey("Id")]
    public class RegionModel : IDataEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public string Name { get; set; }

        public int CountryId { get; set; }

        [ForeignKey("CountryId")]
        public virtual CountryModel Country { get; set; }

        [ForeignKey("RegionId")]
        public virtual ICollection<CityModel> Cities { get; set; }
    }

    [Table("location_Cities")]
    [ScaffoldTable(true)]
    //[DataServiceKey("Id")]
    public class CityModel : IDataEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public string Name { get; set; }

        public int RegionId { get; set; }

        [ForeignKey("RegionId")]
        public virtual RegionModel Region { get; set; }
    }
}