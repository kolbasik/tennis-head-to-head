using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Simple.Core.Data;

namespace Simple.Data.Models
{
    [Table("Competitions")]
    public class CompetitionModel : IDataEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Name { get; set; }
    }
    
    [Table("JustTest")]
    public class JustTestModel : IDataEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Timestamp]
        public byte[] Timestamp { get; set; }

        public string Name { get; set; }

        [DisplayFormat(DataFormatString = "{#,##0.##}", ApplyFormatInEditMode = true, NullDisplayText = "-")]
        public decimal? Decimal { get; set; }

        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime DateTime { get; set; }
    }
}