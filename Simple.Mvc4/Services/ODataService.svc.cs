using System.Data.Services;
using System.Data.Services.Common;
using System.ServiceModel;
using System.ServiceModel.Activation;
using Simple.DAL;

namespace Simple.Mvc4.Services
{
    /// <summary>
    /// http://blogs.msdn.com/b/alexj/archive/2010/06/11/odata-wcf-data-services-best-practices-from-teched.aspx
    /// </summary>
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ODataService : DataService<EntityFrameworkDbContext>
    {
        protected override EntityFrameworkDbContext CreateDataSource()
        {
            var context = new EntityFrameworkDbContext();
            context.Configuration.ProxyCreationEnabled = false;
            return context;
        }

        public static void InitializeService(DataServiceConfiguration config)
        {
            // Allow full access rights on all entity sets
            config.SetEntitySetAccessRule("*", EntitySetRights.AllRead);
            config.SetServiceOperationAccessRule("*", ServiceOperationRights.AllRead);
            config.DataServiceBehavior.MaxProtocolVersion = DataServiceProtocolVersion.V3;

            // return more information in the error response message for troubleshooting
            config.UseVerboseErrors = true;
        }

        /*
                [QueryInterceptor("Countries")]
                public Expression<Func<CountryModel, bool>> CountriesFilter()
                {
                    var user = HttpContext.Current.User.Identity.Name;
                    if (string.IsNullOrEmpty(user))
                        return model => false;
                    else if (user == "Administrator")
                        return o => true;
                    else
                        return model => model.Name == "ukraine";
                }
         
                [ChangeInterceptor("Albums")]
                public void OnChangeProducts(Album album, UpdateOperations operations)
                {
                    if (HttpContext.Current.User.Identity.Name != "Administrator")
                        throw new DataServiceException(403, "Access Denied");
                    if ((operations & UpdateOperations.Add) == UpdateOperations.Add)
                        Debug.WriteLine("Adding an Album");
                    if ((operations & UpdateOperations.Change) == UpdateOperations.Change)
                        Debug.WriteLine("Changing an Album");
                    if ((operations & UpdateOperations.Delete) == UpdateOperations.Delete)
                        Debug.WriteLine("Deleting an Album");
                }

            [OperationContract]
            [WebInvoke(Method="POST", UriTemplate = "UploadFile/{fileName}/{userToken}")]
            string UploadFile(string fileName,string userToken,Stream fileContents);

            // To use HTTP GET, add [WebGet] attribute. (Default ResponseFormat is WebMessageFormat.Json)
            // To create an operation that returns XML,
            //     add [WebGet(ResponseFormat=WebMessageFormat.Xml)],
            //     and include the following line in the operation body:
            //         WebOperationContext.Current.OutgoingResponse.ContentType = "text/xml";
            [OperationContract]
            public string DoWork()
            {
                // Add your operation implementation here
                return "Success";
            }
        */
    }
}
