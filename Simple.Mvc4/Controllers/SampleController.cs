﻿using System.Web.Mvc;

namespace Simple.Mvc4.Controllers
{
    /// <summary>
    /// http://codeshow.codeplex.com
    /// https://www.microsoftvirtualacademy.com/tracks/developing-html5-apps-jump-start?o=1943
    /// </summary>
    public class SampleController : Controller
    {
        public ActionResult Code(string id)
        {
            return View(id);
        }
    }
}
