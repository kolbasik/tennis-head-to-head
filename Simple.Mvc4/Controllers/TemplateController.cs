﻿using System;
using System.Web.Mvc;
using Simple.Mvc4.ViewModels;

namespace Simple.Mvc4.Controllers
{
    public class TemplateController : Controller
    {
        public ActionResult Knockout(string id)
        {
            var viewName = id.Replace('/', '-');
            return PartialView(viewName, TemplateViewModel.Default);
        }

        /// <summary>
        /// Handle an exception if not exists view.
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            var exception = filterContext.Exception;
            if (exception is InvalidOperationException && exception.Message.Contains("partial view"))
            {
                filterContext.ExceptionHandled = true;
            }
            base.OnResultExecuted(filterContext);
        }
    }
}
