﻿using Simple.Core.Data;
using Simple.Data;
using Simple.Data.Models;
using Simple.Web.WebApi;

namespace Simple.Mvc4.Controllers.API
{
    public class CountriesController : GeneralApiController<ITennisContext, CountryModel>
    {
        public CountriesController(IUnitOfWork<ITennisContext> unitOfWork)
            : base(unitOfWork)
        {
        }
    }

    public class RegionsController : GeneralApiController<ITennisContext, RegionModel>
    {
        public RegionsController(IUnitOfWork<ITennisContext> unitOfWork)
            : base(unitOfWork)
        {
        }
    }

    public class CitiesController : GeneralApiController<ITennisContext, CityModel>
    {
        public CitiesController(IUnitOfWork<ITennisContext> unitOfWork)
            : base(unitOfWork)
        {
        }
    }
}