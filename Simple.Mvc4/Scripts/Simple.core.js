(function (window, $, undefined) {
	var modules = {}, templates = {}, router = {}, fn = {};

	var config = $.extend({ }, window['SimpleConfig'], {
		DEBUG: false,
		baseUrl: '/',
		webapiUrl: 'api/v1/',
		templateUrl: 'template/index/'
	});
	
	fn.memoize = function (cache, getter) {
		return function(name) {
			var item = cache[name];
			if (!item) {
				cache[name] = item = getter(name);
			}
			return item;
		};
	};

	var Collection = function (args) {
		var self = this;
		self.name = args.name;
		self.url = function() { return Simple.config.baseUrl + 'api/v1/' + self.name; };
		self.model = args.model || {};
		self.storage = args.storage || [];
	};
	
	var View = function(name) {
		var view = this;
		view.name = name;
	};
	View.prototype.layout = '#layout';
	View.prototype.open = function () { };
	View.prototype.close = function () { };

	var Simple = {
		config: {
			DEBUG: false,
			baseUrl: window._SimpleBaseUrl_ || '/',
			webapiUrl: window._SimpleWebApiUrl_ || 'api/v1/',
			templateUrl: window._SimpleTemplateUrl_ || 'template/index/'
		},
		dom : function(id) {
			return document.getElementById(id);
		},
		module: fn.memoize(modules, function (name) {
			var module = { name : name };
			return module;
		}),
		template: fn.memoize(templates, function (name) {
			var template;
			$.ajax({
				async: false,
				url: Simple.config.baseUrl + Simple.config.templateUrl + name,
				success: function (data) {
					template = data;
				}
			});
			return template || '';
		}),
		collections : {},
		router : router,
		fn: fn
	};
	window['Simple'] = Simple;
	
})(window, window["jQuery"]);


// friend module
(function (s, friend) {

	//...

})(Simple, Simple.module("friend"));
