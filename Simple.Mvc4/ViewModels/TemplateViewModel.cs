﻿namespace Simple.Mvc4.ViewModels
{
    public class TemplateViewModel
    {
        public static readonly TemplateViewModel Default;

        static TemplateViewModel()
        {
            Default = new TemplateViewModel();
        }
    }
}