using System.Data.Entity.Infrastructure;
using System.Web.DynamicData;
using System.Web.Routing;
using System.Web.UI;

namespace Simple.Mvc4
{
    /// <summary>
    /// http://habrahabr.ru/post/150012/
    /// </summary>
    public static class DynamicDataConfig
    {
        public static string RouteUrl = "admin-db";
        public static readonly MetaModel DefaultModel = new MetaModel(true);

        public static void RegisterRoutes(RouteCollection routes)
        {
            DefaultModel.RegisterContext(
                () => ((IObjectContextAdapter)ApplicationFactory.CreateDbContext()).ObjectContext,
                new ContextConfiguration { ScaffoldAllTables = false });

            routes.MapPageRoute("DynamicDataStart", RouteUrl, "~/DynamicData/Default.aspx");
            
            routes.Insert(0,
                new DynamicDataRoute(RouteUrl + "/{table}/{action}.aspx")
                    {
                        Constraints = new RouteValueDictionary(new { action = "List|Details|Edit|Insert" }),
                        Model = DefaultModel
                    });
        }

        public static void RegisterScripts()
        {
            ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition
                {
                    Path = "~/scripts/jquery-1.8.3.min.js",
                    DebugPath = "~/scripts/jquery-1.8.3.js",
                    CdnPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js",
                    CdnDebugPath = "http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.js",
                    CdnSupportsSecureConnection = true,
                    LoadSuccessExpression = "window.jQuery"
                });
        }
    }
}