﻿using System.Web.Mvc;
using Elmah;

namespace Simple.Mvc4
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new ElmahHandleErrorAttribute());
        }

        private class ElmahHandleErrorAttribute : HandleErrorAttribute
        {
            public override void OnException(ExceptionContext context)
            {
                base.OnException(context);
                if (!context.ExceptionHandled)
                    return;
                var httpContext = context.HttpContext.ApplicationInstance.Context;
                ErrorSignal.FromContext(httpContext).Raise(context.Exception, httpContext);
            }
        }
    }
}