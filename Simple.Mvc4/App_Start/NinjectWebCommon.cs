using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Dependencies;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Parameters;
using Ninject.Syntax;
using Ninject.Web.Common;
using Simple.Core.Data;
using Simple.Data;
using Simple.Mvc4.App_Start;

[assembly: WebActivator.PreApplicationStartMethod(typeof (NinjectWebCommon), "Start")]
[assembly: WebActivator.ApplicationShutdownMethod(typeof (NinjectWebCommon), "Stop")]

namespace Simple.Mvc4.App_Start
{
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper Bootstrapper;

        static NinjectWebCommon()
        {
            Bootstrapper = new Bootstrapper();
        }

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof (OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof (NinjectHttpModule));
            
            Bootstrapper.Initialize(CreateKernel);
            
            // tell asp.net mvc to use our Ninject DI Container
            var kernel = Bootstrapper.Kernel;
            var resolver = new NinjectDependencyResolver(kernel);
            GlobalConfiguration.Configuration.DependencyResolver = resolver;
            System.Web.Mvc.DependencyResolver.SetResolver(resolver);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            Bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
            kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

            RegisterServices(kernel);

            return kernel;
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            //kernel.Load(Assembly.GetExecutingAssembly());
            kernel.Bind<IUnitOfWork<ITennisContext>>().ToMethod(ctx => ApplicationFactory.CreateUnitOfWork());
        }
    }

    public class NinjectDependencyResolver : NinjectDependencyScope, System.Web.Http.Dependencies.IDependencyResolver, System.Web.Mvc.IDependencyResolver
    {
        private readonly IKernel _kernel;

        public NinjectDependencyResolver(IKernel kernel)
            : base(kernel)
        {
            _kernel = kernel;
        }

        public IDependencyScope BeginScope()
        {
            return new NinjectDependencyScope(_kernel.BeginBlock());
        }
    }

    public class NinjectDependencyScope : IDependencyScope
    {
        private IResolutionRoot _kernel;

        public NinjectDependencyScope(IResolutionRoot kernel)
        {
            _kernel = kernel;
        }

        public object GetService(Type serviceType)
        {
            if (_kernel == null)
                throw new ObjectDisposedException("this", "This scope has been disposed");

            // return _kernel.TryGet(serviceType);

            var request = CreateRequest(serviceType);
            return _kernel.Resolve(request).SingleOrDefault();
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            if (_kernel == null)
                throw new ObjectDisposedException("this", "This scope has been disposed");

            // return _kernel.GetAll(serviceType);

            var request = CreateRequest(serviceType);
            return _kernel.Resolve(request).ToList();
        }

        private Ninject.Activation.IRequest CreateRequest(Type serviceType)
        {
            return _kernel.CreateRequest(serviceType, null, new Parameter[0], true, true);
        }


        public void Dispose()
        {
            var disposable = _kernel as IDisposable;
            if (disposable != null)
                disposable.Dispose();
            _kernel = null;
        }
    }
}