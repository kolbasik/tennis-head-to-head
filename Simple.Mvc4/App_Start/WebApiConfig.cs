﻿using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json.Serialization;
using Simple.Web.WebApi;

namespace Simple.Mvc4
{
	/// <summary>
	/// http://blogs.msdn.com/b/hongyes/archive/2012/09/02/support-format-in-asp-net-web-api.aspx
	/// http://wildermuth.com/2012/2/22/WebAPI_for_the_MVC_Guy
	/// JSON and XML Serialization in ASP.NET Web API:
	/// http://www.asp.net/web-api/overview/formats-and-model-binding/json-and-xml-serialization#handling_circular_object_references
	/// 
	/// Web API integration (aka Authenticated/authorized web service advise - RESTful?)
	/// http://orchard.codeplex.com/discussions/353559
	/// 
	/// OData support in ASP.NET Web API
	/// http://blogs.msdn.com/b/alexj/archive/2012/08/15/odata-support-in-asp-net-web-api.aspx
	/// 
	/// Tracing in ASP.NET Web API
	/// http://blogs.msdn.com/b/roncain/archive/2012/04/12/tracing-in-asp-net-web-api.aspx
	/// </summary>
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
		{
			bool isdebug = HttpContext.Current.IsDebuggingEnabled;

			config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Never;
			config.Filters.Add(new ValidationFilterAttribute());

			config.Formatters.JsonFormatter.AddQueryStringMapping("$format", "json", "application/json");
			config.Formatters.JsonFormatter.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
			config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver(); 
			// config.Formatters.XmlFormatter.AddQueryStringMapping("$format", "xml", "application/xml");
			config.Formatters.Remove(config.Formatters.XmlFormatter);

			// OData support in ASP.NET Web API
			config.EnableQuerySupport(new ExtendedQueryableAttribute());

			config.Routes.MapHttpRoute(
				name: "DefaultApi",
				routeTemplate: "api/v1/{controller}/{id}",
				defaults: new { id = RouteParameter.Optional }
			);

			if (!isdebug) return;

			config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.LocalOnly;
			config.Formatters.JsonFormatter.Indent = true;

			// To disable tracing in your application, please comment out or remove the following line of code
			// For more information, refer to: http://www.asp.net/web-api
			TraceConfig.Register(config);
		}
	}
}
