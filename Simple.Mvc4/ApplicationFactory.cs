using Simple.Core.Data;
using Simple.DAL;
using Simple.Data;

namespace Simple.Mvc4
{
    public static class ApplicationFactory
    {
        public static IUnitOfWork<ITennisContext> CreateUnitOfWork()
        {
            var context = CreateTennisContext();
            return new UnitOfWork<ITennisContext>(context);
        }

        public static ITennisContext CreateTennisContext()
        {
            var context = CreateDbContext();
            return new EntityFrameworkTennisContext(context);
        }

        public static EntityFrameworkDbContext CreateDbContext()
        {
            return new EntityFrameworkDbContext();
        }
    }
}