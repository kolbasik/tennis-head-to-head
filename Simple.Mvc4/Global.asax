﻿<%@ Application Codebehind="Global.asax.cs" Inherits="Simple.Mvc4.MvcApplication" Language="C#" %>
<%@ Import Namespace="System.Data.Entity" %>
<%@ Import Namespace="System.Web.Http" %>
<%@ Import Namespace="Microsoft.Web.Mvc" %>
<%@ Import Namespace="Simple.DAL" %>
<%@ Import Namespace="Simple.Mvc4" %>
<%@ Import Namespace="Simple.Mvc4.Security" %>

<script runat="server">
	
	protected void Application_Start()
	{
		MvcHandler.DisableMvcResponseHeader = true;
		
		AreaRegistration.RegisterAllAreas();

		FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
		WebApiConfig.Register(GlobalConfiguration.Configuration);
		DynamicDataConfig.RegisterRoutes(RouteTable.Routes);
		DynamicDataConfig.RegisterScripts();
		RouteConfig.RegisterRoutes(RouteTable.Routes);
		BundleConfig.RegisterBundles(BundleTable.Bundles);
		AuthConfig.RegisterAuth();
		SimpleMembershipConfig.RegisterSimpleMembership();

		Database.SetInitializer(new EntityFrameworkDbContextInitializer());

		/* ASP.NET MVC 4 Mobile Caching Bug Fixed
			http://blogs.msdn.com/b/rickandy/archive/2012/09/17/asp-net-mvc-4-mobile-caching-bug-fixed.aspx
		*/
		ViewEngines.Engines.Clear();
		ViewEngines.Engines.Add(new FixedRazorViewEngine());
	}

	/*/// <summary>
	/// http://goneale.com/2008/12/19/lowercase-route-urls-in-aspnet-mvc/
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="args"> </param>
	protected void Application_BeginRequest(Object sender, EventArgs args)
	{
		// If upper case letters are found in the URL, redirect to lower case URL (keep querystring the same).
		var url = Request.Url;
		var lowercaseURL = (url.Scheme + "://" + url.Authority + url.AbsolutePath);
		if (Regex.IsMatch(lowercaseURL, @"[A-Z]"))
		{
			lowercaseURL = lowercaseURL.ToLower() + url.Query;

			Response.Clear();
			Response.Status = "301 Moved Permanently";
			Response.AddHeader("Location", lowercaseURL);
			Response.End();
		}
	}*/

	protected void Application_PreSendRequestHeaders()
	{
		// just for stupid hackers
		var headers = HttpContext.Current.Response.Headers;
		headers.Set("Server", "nginx"); 
		headers.Set("X-UA-Compatible", "IE=edge,chrome=1"); 
	}

</script>