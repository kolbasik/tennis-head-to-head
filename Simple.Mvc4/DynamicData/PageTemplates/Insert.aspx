﻿<%@ Page Language="C#" CodeBehind="Insert.aspx.cs" Inherits="DynamicDataEntities.Insert" %>

<asp:Content ContentPlaceHolderID="HeadPlaceHolder" Runat="Server"></asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
	<asp:DynamicDataManager ID="xDynamicDataManager" runat="server" AutoLoadForeignKeys="true">
		<DataControls>
			<asp:DataControlReference ControlID="FormView1" />
		</DataControls>
	</asp:DynamicDataManager>

	<h2>Add new entry to table <%= table.DisplayName %></h2>

	<asp:UpdatePanel ID="xUpdatePanel" runat="server">
		<ContentTemplate>
			<asp:ValidationSummary ID="xValidationSummary" runat="server" EnableClientScript="true" HeaderText="List of validation errors" />
			<asp:DynamicValidator runat="server" ID="DetailsViewValidator" ControlToValidate="FormView1" />

			<asp:FormView ID="FormView1" runat="server" DataSourceID="DetailsDataSource" DefaultMode="Insert"
					OnItemCommand="FormView1_ItemCommand" OnItemInserted="FormView1_ItemInserted" RenderOuterTable="false">
				<InsertItemTemplate>
					<asp:DynamicEntity runat="server" Mode="Insert" />
					<div>
						<asp:LinkButton runat="server" CommandName="Insert" Text="Insert" CssClass="btn btn-primary" />
						<asp:LinkButton runat="server" CommandName="Cancel" Text="Cancel" CausesValidation="false" CssClass="btn btn-link" />
					</div>
				</InsertItemTemplate>
			</asp:FormView>

			<asp:EntityDataSource ID="DetailsDataSource" runat="server" EnableInsert="true" />
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

