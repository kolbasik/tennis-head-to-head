﻿<%@ Page Language="C#" CodeBehind="Edit.aspx.cs" Inherits="DynamicDataEntities.Edit" %>

<asp:Content ContentPlaceHolderID="HeadPlaceHolder" Runat="Server"></asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
	<asp:DynamicDataManager ID="xDynamicDataManager" runat="server" AutoLoadForeignKeys="true">
		<DataControls>
			<asp:DataControlReference ControlID="FormView1" />
		</DataControls>
	</asp:DynamicDataManager>

	<h2>Edit entry from table <%= table.DisplayName %></h2>

	<asp:UpdatePanel ID="xUpdatePanel" runat="server">
		<ContentTemplate>
			<asp:ValidationSummary ID="xValidationSummary" runat="server" EnableClientScript="true" HeaderText="List of validation errors" />
			<asp:DynamicValidator runat="server" ID="DetailsViewValidator" ControlToValidate="FormView1" />

			<asp:FormView runat="server" ID="FormView1" DataSourceID="DetailsDataSource" DefaultMode="Edit"
						OnItemCommand="FormView1_ItemCommand" OnItemUpdated="FormView1_ItemUpdated" RenderOuterTable="false">
				<EditItemTemplate>
					<asp:DynamicEntity runat="server" Mode="Edit" />
					<div>
						<asp:LinkButton runat="server" CommandName="Update" Text="Update" CssClass="btn btn-primary" />
						<asp:LinkButton runat="server" CommandName="Cancel" Text="Cancel" CausesValidation="false" CssClass="btn btn-link" />
					</div>
				</EditItemTemplate>
			</asp:FormView>

			<asp:EntityDataSource ID="DetailsDataSource" runat="server" EnableUpdate="true" />

			<asp:QueryExtender TargetControlID="DetailsDataSource" ID="DetailsQueryExtender" runat="server">
				<asp:DynamicRouteExpression />
			</asp:QueryExtender>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>

