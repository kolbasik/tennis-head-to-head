﻿<%@ Page Language="C#" CodeBehind="Details.aspx.cs" Inherits="DynamicDataEntities.Details" %>

<asp:Content ContentPlaceHolderID="HeadPlaceHolder" Runat="Server"></asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
	<asp:DynamicDataManager ID="xDynamicDataManager" runat="server" AutoLoadForeignKeys="true">
		<DataControls>
			<asp:DataControlReference ControlID="FormView1" />
		</DataControls>
	</asp:DynamicDataManager>

	<h2>Entry from table <%= table.DisplayName %></h2>

	<asp:UpdatePanel ID="xUpdatePanel" runat="server">
		<ContentTemplate>
			<asp:EntityDataSource runat="server" ID="DetailsDataSource" EnableDelete="true" />
			<asp:QueryExtender runat="server" ID="DetailsQueryExtender" TargetControlID="DetailsDataSource">
				<asp:DynamicRouteExpression />
			</asp:QueryExtender>

			<div>
				<asp:ValidationSummary ID="xValidationSummary" runat="server" EnableClientScript="true" HeaderText="List of validation errors" />
				<asp:DynamicValidator runat="server" ID="DetailsViewValidator" ControlToValidate="FormView1" />
			</div>

			<asp:FormView runat="server" ID="FormView1" DataSourceID="DetailsDataSource" OnItemDeleted="FormView1_ItemDeleted" RenderOuterTable="false">
				<ItemTemplate>
					<div>
						<asp:DynamicHyperLink ID="DynamicHyperLink1" runat="server" Action="Edit" Text="Edit" CssClass="btn btn-link" />
						<asp:LinkButton ID="LinkButton1" runat="server" CommandName="Delete" Text="Delete" CssClass="btn btn-link" OnClientClick='return confirm("Are you sure you want to delete this item?");' />
					</div>
					<asp:DynamicEntity runat="server" />
				</ItemTemplate>
			</asp:FormView>

			<div>
				<asp:DynamicHyperLink ID="ListHyperLink" runat="server" Action="List" CssClass="btn btn-link">Show all items</asp:DynamicHyperLink>
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>