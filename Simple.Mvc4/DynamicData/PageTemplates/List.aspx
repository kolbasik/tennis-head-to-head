﻿<%@ Page Language="C#" CodeBehind="List.aspx.cs" Inherits="DynamicDataEntities.List" %>

<asp:Content ContentPlaceHolderID="HeadPlaceHolder" Runat="Server"></asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
	<asp:DynamicDataManager ID="xDynamicDataManager" runat="server" AutoLoadForeignKeys="true">
		<DataControls>
			<asp:DataControlReference ControlID="GridView1" />
		</DataControls>
	</asp:DynamicDataManager>
	<asp:EntityDataSource runat="server" ID="GridDataSource" EnableDelete="true" />
	<asp:QueryExtender runat="server" ID="GridQueryExtender" TargetControlID="GridDataSource">
		<asp:DynamicFilterExpression ControlID="FilterRepeater" />
	</asp:QueryExtender>

	<h2><%= table.DisplayName %></h2>

	<asp:UpdatePanel ID="xUpdatePanel" runat="server">
		<ContentTemplate>
			<div>
				<asp:ValidationSummary runat="server" ID="xValidationSummary" EnableClientScript="true" HeaderText="List of validation errors" />
				<asp:DynamicValidator runat="server" ID="GridViewValidator" ControlToValidate="GridView1" />
				<asp:QueryableFilterRepeater runat="server" ID="FilterRepeater">
					<ItemTemplate>
						<div class="control-group">
							<asp:Label runat="server" Text='<%# Eval("DisplayName") %>' OnPreRender="Label_PreRender" CssClass="control-label" />
							<div class="controls">
								<asp:DynamicFilter runat="server" ID="DynamicFilter" OnFilterChanged="DynamicFilter_FilterChanged" />
							</div>
						</div>
					</ItemTemplate>
				</asp:QueryableFilterRepeater>
			</div>
			
			<div>
				<asp:GridView runat="server" ID="GridView1" DataSourceID="GridDataSource" EnablePersistedSelection="true" AllowPaging="True" AllowSorting="True">
					<Columns>
						<asp:TemplateField HeaderStyle-Width="120px">
							<ItemTemplate>
								<asp:DynamicHyperLink runat="server" Action="Edit" Text="Edit" />
								<asp:LinkButton runat="server" CommandName="Delete" Text="Delete" OnClientClick='return confirm("Are you sure you want to delete this item?");'/>
								<asp:DynamicHyperLink runat="server" Text="Details" />
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
				</asp:GridView>
			</div>
			
			<div>
				<asp:DynamicHyperLink runat="server" ID="InsertHyperLink" Action="Insert" CssClass="btn btn-primary">Insert new item</asp:DynamicHyperLink>
			</div>
		</ContentTemplate>
	</asp:UpdatePanel>
</asp:Content>