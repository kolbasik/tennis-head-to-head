﻿<%@ Control Language="C#" CodeBehind="Default_Insert.ascx.cs" Inherits="DynamicDataEntities.Default_InsertEntityTemplate" %>
<%@ Reference Control="~/DynamicData/EntityTemplates/Default.ascx" %>
<asp:EntityTemplate runat="server" ID="EntityTemplate1">
	<ItemTemplate>
		<dt>
			<%--<asp:Label runat="server" OnInit="Label_Init" OnPreRender="Label_PreRender" />--%>
			<asp:Label runat="server" OnInit="Label_Init" />
		</dt>
		<dd>
			<asp:DynamicControl runat="server" ID="DynamicControl" Mode="Insert" OnInit="DynamicControl_Init" />
		</dd>
	</ItemTemplate>
</asp:EntityTemplate>