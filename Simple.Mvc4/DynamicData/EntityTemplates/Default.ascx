﻿<%@ Control Language="C#" CodeBehind="Default.ascx.cs" Inherits="DynamicDataEntities.DefaultEntityTemplate" %>

<asp:EntityTemplate runat="server" ID="EntityTemplate1">
	<ItemTemplate>
		<div class="control-group">
			<asp:Label runat="server" OnInit="Label_Init" CssClass="control-label" />
			<div class="controls">
				<asp:DynamicControl runat="server" OnInit="DynamicControl_Init" />
			</div>
		</div>
	</ItemTemplate>
</asp:EntityTemplate>

