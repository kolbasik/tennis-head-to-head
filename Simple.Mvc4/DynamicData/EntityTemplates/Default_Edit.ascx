﻿<%@ Control Language="C#" CodeBehind="Default_Edit.ascx.cs" Inherits="DynamicDataEntities.Default_EditEntityTemplate" %>
<%@ Reference Control="~/DynamicData/EntityTemplates/Default.ascx" %>
<asp:EntityTemplate runat="server" ID="EntityTemplate1">
	<ItemTemplate>
		<div class="control-group">
			<asp:Label runat="server" OnInit="Label_Init" OnPreRender="Label_PreRender" CssClass="control-label" />
			<div class="controls">
				<asp:DynamicControl runat="server" ID="DynamicControl" Mode="Edit" OnInit="DynamicControl_Init" />
			</div>
		</div>
	</ItemTemplate>
</asp:EntityTemplate>