﻿<%@ Page Language="C#" CodeBehind="Default.aspx.cs" Inherits="Simple.Mvc4.DynamicData._Default" %>

<asp:Content ContentPlaceHolderID="HeadPlaceHolder" Runat="Server"></asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
	<asp:ScriptManagerProxy ID="xScriptManagerProxy" runat="server" />
	
	<section>
		<header>
			<h2>List of tables</h2>
		</header>
		<asp:GridView ID="xMenu" runat="server" AutoGenerateColumns="false">
			<Columns>
				<asp:TemplateField HeaderText="Table Name" SortExpression="TableName">
					<ItemTemplate>
						<asp:DynamicHyperLink runat="server"><%# Eval("DisplayName") %></asp:DynamicHyperLink>
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</asp:GridView>
	</section>

</asp:Content>


