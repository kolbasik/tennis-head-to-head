﻿using System;
using System.Web.DynamicData;

namespace Simple.Mvc4.DynamicData
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            System.Collections.IList visibleTables = MetaModel.Default.VisibleTables;
            if (visibleTables.Count == 0)
            {
                throw new InvalidOperationException("There are no accessible tables. Make sure that at least one data model is registered in Global.asax and scaffolding is enabled or implement custom pages.");
            }
            xMenu.DataSource = visibleTables;
            xMenu.DataBind();
        }
    }
}
