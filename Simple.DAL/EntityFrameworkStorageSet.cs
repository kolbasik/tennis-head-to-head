using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using Simple.Core.Data;

namespace Simple.DAL
{
    public class EntityFrameworkStorageSet<TEntity> : IDataSet<TEntity>
        where TEntity : class, IDataEntity
    {
        private readonly DbQuery<TEntity> _dbSet;
        private readonly IQueryable<TEntity> _query;

        public EntityFrameworkStorageSet(DbQuery<TEntity> dbSet)
        {
            if (dbSet == null) throw new ArgumentNullException("dbSet");
            _dbSet = dbSet;
            _query = dbSet;
        }

        public IEnumerator<TEntity> GetEnumerator()
        {
            return _query.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Expression Expression
        {
            get { return _query.Expression; }
        }
        
        public Type ElementType
        {
            get { return _query.ElementType; }
        }

        public IQueryProvider Provider
        {
            get { return _query.Provider; }
        }

        IDataSet IDataSet.Include(string path)
        {
            return Include(path);
        }

        public IDataSet<TEntity> Include(string path)
        {
            var query = _dbSet.Include(path);
            return new EntityFrameworkStorageSet<TEntity>(query);
        }

        public IDataSet<TEntity> Include<TProperty>(Expression<Func<TEntity, TProperty>> property)
        {
            var path = DbHelper.ParsePath(property);
            return Include(path);
        }
    }
}