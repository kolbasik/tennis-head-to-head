using System;
using System.Collections.Concurrent;
using Simple.Core.Data;

namespace Simple.DAL
{
    public sealed class EntityFrameworkReposioryResolver
    {
        private readonly EntityFrameworkDbContext _context;
        private readonly ConcurrentDictionary<Type, object> _cache;

        public EntityFrameworkReposioryResolver(EntityFrameworkDbContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            _context = context;
            _cache = new ConcurrentDictionary<Type, object>();
        }

        public EntityFrameworkRepository<TEntity> Resolve<TEntity>() where TEntity : class, IDataEntity
        {
            var repository = _cache.GetOrAdd(typeof (TEntity), CreateDefaultRepository<TEntity>);
            return (EntityFrameworkRepository<TEntity>)repository;
        }

        private object CreateDefaultRepository<TEntity>(Type type) where TEntity : class, IDataEntity
        {
            return new EntityFrameworkRepository<TEntity>(_context);
        }
    }
}