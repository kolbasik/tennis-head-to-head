﻿using System.Collections.ObjectModel;
using System.Data.Entity;
using Simple.Data.Models;

namespace Simple.DAL
{
    public class EntityFrameworkDbContextInitializer : DropCreateDatabaseIfModelChanges<EntityFrameworkDbContext>
    {
        protected override void Seed(EntityFrameworkDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );

            var ukraine = new CountryModel
            {
                Name = "Ukraine",
                Regions = new Collection<RegionModel>
                        {
                            new RegionModel
                                {
                                    Name = "Kyiv Region",
                                    Cities = new Collection<CityModel>
                                        {
                                            new CityModel { Name = "Kyiv" },
                                            new CityModel { Name = "Brovary" }
                                        }
                                },
                            new RegionModel
                                {
                                    Name = "Donetsk Region",
                                    Cities = new Collection<CityModel>
                                        {
                                            new CityModel { Name = "Donetsk" },
                                            new CityModel { Name = "Makeevka" }
                                        }
                                }
                        }
            };
            context.Countries.Add(ukraine);

            var russia = new CountryModel
            {
                Name = "Russia",
                Regions = new Collection<RegionModel>
                        {
                            new RegionModel
                                {
                                    Name = "Moscow Region",
                                    Cities = new Collection<CityModel>
                                        {
                                            new CityModel { Name = "Moscow" }
                                        }
                                }
                        }
            };
            context.Countries.Add(russia);

            context.SaveChanges();

            base.Seed(context);
        }
    }

}
