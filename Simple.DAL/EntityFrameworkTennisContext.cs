using System;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Linq;
using Simple.Core.Data;
using Simple.Data;

namespace Simple.DAL
{
    public sealed class EntityFrameworkTennisContext : ITennisContext
    {
        private readonly EntityFrameworkDbContext _context;
        private readonly EntityFrameworkReposioryResolver _resolver;

        public EntityFrameworkTennisContext(EntityFrameworkDbContext context, EntityFrameworkReposioryResolver resolver = null)
        {
            if (context == null) throw new ArgumentNullException("context");
            _context = context;
            _resolver = resolver ?? new EntityFrameworkReposioryResolver(context);
        }

        public IDataRepository<TEntity> GetRepositoryFor<TEntity>() where TEntity : class, IDataEntity
        {
            // if you want to have custom repository, you should define it here
            return _resolver.Resolve<TEntity>();
        }

        public int SaveChanges()
        {
            if (!_context.ChangeTracker.Entries().Any(HasChanged))
                return -1;
            var affected = 0;
            using (var transaction = _context.Database.Connection.BeginTransaction())
            {
                var saveFailed = false;
                do
                {
                    try
                    {
                        affected += _context.SaveChanges();
                        transaction.Commit();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        saveFailed = true;
                        foreach (var entity in ex.Entries)
                        {
                            var databaseValues = entity.GetDatabaseValues();
                            entity.OriginalValues.SetValues(databaseValues);
                        }
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        throw;
                    }
                } while (saveFailed);
            }
            return affected;
        }

        public void Dispose()
        {
            _context.Dispose();
        }

        private static bool HasChanged(DbEntityEntry entity)
        {
            return IsState(entity, EntityState.Added) ||
                   IsState(entity, EntityState.Deleted) ||
                   IsState(entity, EntityState.Modified);
        }

        private static bool IsState(DbEntityEntry entity, EntityState state)
        {
            return (entity.State & state) == state;
        }
    }
}