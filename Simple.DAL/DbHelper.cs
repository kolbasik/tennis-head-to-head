using System;
using System.Linq.Expressions;

namespace Simple.DAL
{
    internal static class DbHelper
    {
        public static string ParsePath<T, TProperty>(Expression<Func<T, TProperty>> path) where T : class
        {
            string result;
            if (!TryParsePath(path.Body, out result) || result == null)
                throw new ArgumentException("Invalid Include Path Expression", "path");
            else
                return result;
        }

        private static bool TryParsePath(Expression expression, out string path)
        {
            path = null;
            expression = RemoveConvert(expression);
            var memberExpression = expression as MemberExpression;
            var methodCallExpression = expression as MethodCallExpression;
            if (memberExpression != null)
            {
                string name = memberExpression.Member.Name;
                string parsed;
                if (!TryParsePath(memberExpression.Expression, out parsed))
                    return false;
                path = parsed == null ? name : parsed + "." + name;
            }
            else if (methodCallExpression != null)
            {
                string name;
                if (methodCallExpression.Method.Name == "Select" && methodCallExpression.Arguments.Count == 2 && (TryParsePath(methodCallExpression.Arguments[0], out name) && name != null))
                {
                    var lambdaExpression = methodCallExpression.Arguments[1] as LambdaExpression;
                    string parsed;
                    if (lambdaExpression != null && TryParsePath(lambdaExpression.Body, out parsed) && parsed != null)
                    {
                        path = name + "." + parsed;
                        return true;
                    }
                }
                return false;
            }
            return true;
        }

        private static Expression RemoveConvert(Expression expression)
        {
            while (expression != null && (expression.NodeType == ExpressionType.Convert || expression.NodeType == ExpressionType.ConvertChecked))
                expression = RemoveConvert(((UnaryExpression)expression).Operand);
            return expression;
        }
    }
}