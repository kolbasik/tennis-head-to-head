using System.Data.Entity;
using Simple.Data.Models;

namespace Simple.DAL
{
    /// <summary>
    /// http://www.odata.org/documentation/uri-conventions
    /// http://www.odata.org/media/30002/OData.html#theformatsystemqueryoption
    /// Example : http://localhost:14815/OData/TennisService.svc/CountrySet/?$format=json&$expand=Regions/Cities
    /// http://weblogs.asp.net/cibrax/archive/2010/10/08/asp-net-mvc-wcf-rest-and-data-services-when-to-use-what-for-restful-services.aspx
    /// http://net.tutsplus.com/tutorials/building-an-asp-net-mvc4-application-with-ef-and-webapi/
    /// </summary>
    public class EntityFrameworkDbContext : DbContext
    {
        public EntityFrameworkDbContext()
            : this(Contracts.DefaultConnection)
        {
        }

        public EntityFrameworkDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            Configuration.LazyLoadingEnabled = false;
            Configuration.ValidateOnSaveEnabled = false;
            Configuration.ProxyCreationEnabled = false;
            Configuration.AutoDetectChangesEnabled = false;
        }

        public DbSet<CountryModel> Countries { get; set; }
        public DbSet<RegionModel> Regions { get; set; }
        public DbSet<CityModel> Cities { get; set; }
    }
}