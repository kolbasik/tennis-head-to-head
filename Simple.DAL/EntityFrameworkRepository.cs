using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using Simple.Core.Data;

namespace Simple.DAL
{
    public class EntityFrameworkRepository<TEntity> : IDataRepository<TEntity>
        where TEntity : class, IDataEntity
    {
        protected readonly DbContext Context;
        protected readonly DbSet<TEntity> DbSet;

        public EntityFrameworkRepository(DbContext context)
        {
            if (context == null) throw new ArgumentNullException("context");
            Context = context;
            DbSet = context.Set<TEntity>();
        }

        public IDataSet<TEntity> GetAll()
        {
            return new EntityFrameworkStorageSet<TEntity>(DbSet);
        }

        public TEntity GetById(int id)
        {
            return DbSet.Find(id);
        }

        public void Insert(TEntity entity)
        {
            DbSet.Add(entity);
            Context.Entry(entity).State = EntityState.Added;
        }

        public void Update(TEntity entity)
        {
            DbSet.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            var entity = DbSet.Find(id);
            if (entity != null)
            {
                Delete(entity);
            }
        }

        public void Delete(TEntity entity)
        {
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            DbSet.Remove(entity);
        }

        public void Save()
        {
            Context.SaveChanges();
        }

        public IEnumerable<TEntity> GetWithRawSql(string query, params object[] parameters)
        {
            return DbSet.SqlQuery(query, parameters);
        }

        public void Dispose()
        {
            // TODO: needs to think about it
            // Context.Dispose();
        }
    }
}