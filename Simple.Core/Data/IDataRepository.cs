using System;

namespace Simple.Core.Data
{
    public interface IDataRepository<TEntity> : IDataRepository<TEntity, int>
        where TEntity : class, IDataEntity
    {
    }

    public interface IDataRepository<TEntity, in TKey> : IDisposable
        where TEntity : class, IDataEntity
        where TKey : struct
    {
        IDataSet<TEntity> GetAll();
        TEntity GetById(TKey id);
        void Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(TKey id);
        void Delete(TEntity entity);
        void Save();
    }
}