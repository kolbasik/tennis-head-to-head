﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Simple.Core.Data
{
	/// <summary>
	/// http://ayende.com/blog/4784/architecting-in-the-pit-of-doom-the-evils-of-the-repository-abstraction-layer
	/// </summary>
	/// <typeparam name="TEntity"></typeparam>
	public interface IDataSpecification<TEntity> where TEntity : class, IDataEntity
	{
		Expression<Func<TEntity, bool>> MatchingCriteria { get; }
		IEnumerable<Expression<Func<TEntity, bool>>> FetchPaths { get; }
	}
}