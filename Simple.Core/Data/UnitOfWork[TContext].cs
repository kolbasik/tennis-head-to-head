namespace Simple.Core.Data
{
    public sealed class UnitOfWork<TContext> : Disposable, IUnitOfWork<TContext> 
        where TContext : IDataContext
    {
        public UnitOfWork(TContext context)
        {
            Context = context;
        }

        public TContext Context { get; private set; }

        public void Commit()
        {
            Context.SaveChanges();
        }

        protected override void DisposeCore()
        {
            Context.Dispose();
        }
    }
}