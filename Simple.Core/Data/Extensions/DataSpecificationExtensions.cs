using System.Linq;

namespace Simple.Core.Data.Extensions
{
    public static class DataSpecificationExtensions
    {
        public static IQueryable<TEntity> Prepare<TEntity>(this IDataSet<TEntity> dataSet, IDataSpecification<TEntity> specification)
            where TEntity : class, IDataEntity
        {
            if (specification == null)
            {
                return dataSet;
            }
            IDataSet<TEntity> query = specification.FetchPaths.Aggregate(dataSet, (current, fetchPath) => current.Include(fetchPath));
            return query.Where(specification.MatchingCriteria);
        }
    }
}