using System;

namespace Simple.Core.Data
{
    public interface IDataContext : IDisposable
    {
        IDataRepository<TEntity> GetRepositoryFor<TEntity>() where TEntity : class, IDataEntity;
        int SaveChanges();
    }
}