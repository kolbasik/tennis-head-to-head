using System;
using System.Linq;
using System.Linq.Expressions;

namespace Simple.Core.Data
{
    public interface IDataSet : IQueryable
    {
        IDataSet Include(string property);
    }

    public interface IDataSet<TEntity> : IDataSet, IQueryable<TEntity>
        where TEntity : class, IDataEntity
    {
        new IDataSet<TEntity> Include(string property);
        IDataSet<TEntity> Include<TProperty>(Expression<Func<TEntity, TProperty>> property);
    }
}