namespace Simple.Core.Data
{
    public interface IUnitOfWork<out TContext> : IUnitOfWork
        where TContext : IDataContext
    {
        TContext Context { get; }
    }
}