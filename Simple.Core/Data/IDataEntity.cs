namespace Simple.Core.Data
{
    public interface IDataEntity
    {
        int Id { get; set; }
    }
}