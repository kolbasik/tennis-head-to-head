﻿using System;

namespace Simple.Core
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
    }
}
