﻿using System;
using System.Threading.Tasks;

namespace Simple.Core.ServiceBus
{
    public interface IBus
    {
        void Send<T>(T message) where T : IMessage;
        Task SendAsync<T>(T message) where T : IMessage;
    }

    public class Bus : IBus
    {
        private readonly IServiceProvider _provider;

        public Bus(IServiceProvider provider)
        {
            if (provider == null) throw new ArgumentNullException("provider");
            _provider = provider;
        }

        public void Send<T>(T message) where T : IMessage
        {
            var handler = GetMessageHandler<T>();
            handler.Handle(message);
        }

        public Task SendAsync<T>(T message) where T : IMessage
        {
            return Task.Run(() => Send(message));
        }

        private IMessageHandler<T> GetMessageHandler<T>() where T : IMessage
        {
            var handler = _provider.GetService(typeof(IMessageHandler<T>)) as IMessageHandler<T>;
            if (handler == null)
            {
                var message = string.Format("Any handler for '{0}' message is not found.", typeof(T).FullName);
                throw new InvalidOperationException(message);
            }
            return handler;
        }
    }
}