namespace Simple.Core.ServiceBus
{
    internal class SampleMessage : IMessage<bool>
    {
        public int Id { get; set; }
        public bool Result { get; set; }
    }

    internal class SampleMessageHandler : IMessageHandler<SampleMessage>
    {
        public void Handle(SampleMessage message)
        {
            message.Result = message.Id % 2 == 0;
        }
    }
}