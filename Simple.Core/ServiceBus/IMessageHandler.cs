namespace Simple.Core.ServiceBus
{
    public interface IMessageHandler<in TMessage>
        where TMessage : IMessage
    {
        void Handle(TMessage message);
    }
}