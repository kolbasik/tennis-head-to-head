namespace Simple.Core.ServiceBus
{
    public interface IMessage
    {
    }

    public interface IMessage<TResult> : IMessage
    {
        TResult Result { get; set; }
    }
}