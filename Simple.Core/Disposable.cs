using System;

namespace Simple.Core
{
    public abstract class Disposable : IDisposable
    {
        private bool _disposed;

        ~Disposable()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    DisposeCore();
                }
            }
            _disposed = true;
        }

        protected abstract void DisposeCore();
    }
}