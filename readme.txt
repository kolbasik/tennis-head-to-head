﻿http://localhost:14815/api/v1/countries?$expand=regions/cities
http://localhost:14815/dynamic-data/

Building an ASP.NET MVC4 Application with EF and WebAPI
http://net.tutsplus.com/tutorials/building-an-asp-net-mvc4-application-with-ef-and-webapi/

Responsive background images with fixed or fluid aspect ratios
http://voormedia.com/blog/2012/11/responsive-background-images-with-fixed-or-fluid-aspect-ratios

Sample Mvc Site
	BEST: https://github.com/NuGet/NuGetGallery
	http://www.calabonga.net/blog/all/all/index/2?tag=%D0%BC%D1%83%D0%B7%D0%B5%D0%B9%20%D1%8E%D0%BC%D0%BE%D1%80%D0%B0

	Lowercase Route URL's in ASP.NET MVC
		http://goneale.com/2008/12/19/lowercase-route-urls-in-aspnet-mvc/

	Route Constraints: Working with conflicting routes in MVC
		http://www.babel-lutefisk.net/2011/11/route-constraints-working-with.html	

	ASP.NET Error Handling
		http://www.asp.net/web-forms/tutorials/aspnet-45/getting-started-with-aspnet-45-web-forms/aspnet-error-handling

Tools:
	Test Cover For .Net:
		https://github.com/sawilde/opencover
	Continuous Tests
		http://continuoustests.com/

METRO UI
	Milk - CSS framework для быстрой верстки like Metro UI
		http://milk.ecm7.ru/
		http://habrahabr.ru/post/159057/
		
Profiling
	MiniProfiler
		http://www.hanselman.com/blog/NuGetPackageOfTheWeek9ASPNETMiniProfilerFromStackExchangeRocksYourWorld.aspx

WebApi
	OData support
		http://blogs.msdn.com/b/alexj/archive/2012/08/15/odata-support-in-asp-net-web-api.aspx
		http://blogs.msdn.com/b/alexj/archive/2012/08/21/web-api-queryable-current-support-and-tentative-roadmap.aspx
		http://www.asp.net/web-api/overview/getting-started-with-aspnet-web-api/tutorial-your-first-web-api
		http://weblogs.asp.net/scottgu/archive/2012/02/23/asp-net-web-api-part-1.aspx

	Dependency Resolver
		NInject
			http://www.peterprovost.org/blog/2012/06/19/adding-ninject-to-web-api/
		Unity
			http://www.asp.net/web-api/overview/extensibility/using-the-web-api-dependency-resolver

	File Upload and Multipart MIME
		http://www.asp.net/web-api/overview/working-with-http/sending-html-form-data,-part-2
		
WCF
	Transferring large files using WCF
		http://garfoot.com/blog/2008/06/transferring-large-files-using-wcf/
		http://www.codeguru.com/csharp/.net/net_wcf/article.php/c18583/Passing-Large-Files-in-Windows-Communication-Foundation-WCF-using-Streaming-and-TCP.htm

SPA
	http://aspnetwebstack.codeplex.com/discussions/topics/5321/asp-net-single-page-application
	http://blogs.msdn.com/b/hongyes/archive/2012/08/30/single-page-application-with-backbone-js-and-asp-net-web-api.aspx
	https://github.com/alessioalex/ClientManager/blob/master/app.js
	http://stackoverflow.com/questions/8002828/large-backbone-js-web-app-organization
	https://github.com/nrabinowitz/gapvis/
	http://stackoverflow.com/questions/9473714/mvc-4-single-page-application-and-datetime