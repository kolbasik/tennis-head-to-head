﻿using System.CodeDom;
using System.Web.Compilation;
using System.Web.UI;

namespace Simple.Web.WebForms.Compilation
{
    /// <summary>
    /// http://weblogs.asp.net/infinitiesloop/archive/2006/08/09/The-CodeExpressionBuilder.aspx
    /// <example>
    /// Markup
    ///     &gt;asp:CheckBox id="chk1" runat="server" Text="&gt;%$ Code: DateTime.Now %>"/>
    /// web.config
    ///     &gt;compilation debug="true">
    ///         &gt;expressionBuilders>
    ///             &gt;add expressionPrefix="Code" type="Simple.Web.WebForms.Compilation.CodeExpressionBuilder"/>
    ///         &gt;/expressionBuilders>
    ///     &gt;/compilation>
    /// </example>
    /// </summary>
    [ExpressionPrefix("Code")]
    public class CodeExpressionBuilder : ExpressionBuilder
    {
        public override CodeExpression GetCodeExpression(BoundPropertyEntry entry, object parsedData, ExpressionBuilderContext context)
        {
            return new CodeSnippetExpression(entry.Expression);
        }
    }
}