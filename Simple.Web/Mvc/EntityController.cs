using System;
using System.Data;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Simple.Core.Data;
using Simple.Core.Data.Extensions;

namespace Simple.Web.Mvc
{
    [Flags]
    public enum EntityOperationType
    {
        None,
        Create,
        Read,
        Update,
        Delete,
        All = Create | Read | Update | Delete
    }

    public abstract class EntityController<TContext, TEntity> : GeneralController<TContext>
        where TContext : IDataContext
        where TEntity : class, IDataEntity
    {
        protected EntityOperationType OperationType { get; set; }
        protected IDataRepository<TEntity> Repository { get; set; }

        protected EntityController(IUnitOfWork<TContext> unitOfWork)
            : base(unitOfWork)
        {
            Repository = unitOfWork.Context.GetRepositoryFor<TEntity>();
        }

        // TODO: needs to implement some filter functionality
        /// <summary>
        /// http://www.asp.net/mvc/tutorials/getting-started-with-ef-using-mvc/sorting-filtering-and-paging-with-the-entity-framework-in-an-asp-net-mvc-application
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        public ActionResult Index(int? page)
        {
            EnsurePermission(EntityOperationType.Read);
            var searchCriteria = GetSearchCriteriaFrom(Request);
            var entities = Repository.GetAll().Prepare(searchCriteria);

            /*int pageSize = 25;
            int pageIndex = Math.Max(page ?? 1, 1) - 1;
            return View(entities.ToPagedList(pageIndex, pageSize)); */

            return View(entities);
        }

        [HttpGet]
        public ActionResult Create()
        {
            EnsurePermission(EntityOperationType.Create);
            var entity = CreateEntity();
            return View(entity);
        }

        [HttpPut, ValidateAntiForgeryToken]
        public ActionResult Create(TEntity entity)
        {
            EnsurePermission(EntityOperationType.Create);
            try
            {
                if (ModelState.IsValid)
                {
                    Repository.Insert(entity);
                    Repository.Save();

                    return GoToIndex();
                }
            }
            catch (Exception ex)
            {
                if (HandleException(ex) == false)
                    throw;
            }

            return View(entity);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            EnsurePermission(EntityOperationType.Update);
            var entity = Repository.GetById(id);
            return View(entity);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult Edit(TEntity entity)
        {
            EnsurePermission(EntityOperationType.Update);
            try
            {
                if (ModelState.IsValid)
                {
                    Repository.Update(entity);
                    Repository.Save();

                    return GoToIndex();
                }
            }
            catch (Exception ex)
            {
                if (HandleException(ex) == false)
                    throw;
            }

            return View(entity);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            EnsurePermission(EntityOperationType.Delete);
            var entity = Repository.GetById(id);
            if (entity == null)
            {
                throw new InvalidOperationException("Can't find entity in storage.");
            }
            return View(entity);
        }

        [HttpDelete, ValidateAntiForgeryToken, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id, bool? confirmed)
        {
            EnsurePermission(EntityOperationType.Delete);
            try
            {
                if (confirmed.GetValueOrDefault())
                {
                    Repository.Delete(id);

                    return GoToIndex();
                }
            }
            catch (Exception ex)
            {
                if (HandleException(ex) == false)
                    throw;
            }

            return RedirectToAction("Delete", new RouteValueDictionary {{"id", id}});
        }

        protected virtual void EnsurePermission(EntityOperationType operationType)
        {
            if ((OperationType & operationType) == operationType) return;
            var message = string.Format("You don't have permission for {0} operation.", operationType);
            throw new InvalidOperationException(message);
        }

        protected virtual IDataSpecification<TEntity> GetSearchCriteriaFrom(HttpRequestBase request)
        {
            return null;
        }

        protected virtual TEntity CreateEntity()
        {
            return Activator.CreateInstance<TEntity>();
        }

        protected virtual bool HandleException(Exception ex)
        {
            if (ex is DataException)
            {
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                return true;
            }

            /*catch (DbUpdateConcurrencyException ex)
            {
                var entry = ex.Entries.Single();
                var databaseValues = (Department)entry.GetDatabaseValues().ToObject();
                var clientValues = (Department)entry.Entity;
                if (databaseValues.Name != clientValues.Name)
                    ModelState.AddModelError("Name", "Current value: "
                        + databaseValues.Name);
                if (databaseValues.Budget != clientValues.Budget)
                    ModelState.AddModelError("Budget", "Current value: "
                        + String.Format("{0:c}", databaseValues.Budget));
                if (databaseValues.StartDate != clientValues.StartDate)
                    ModelState.AddModelError("StartDate", "Current value: "
                        + String.Format("{0:d}", databaseValues.StartDate));
                if (databaseValues.InstructorID != clientValues.InstructorID)
                    ModelState.AddModelError("InstructorID", "Current value: "
                        + db.Instructors.Find(databaseValues.InstructorID).FullName);
                ModelState.AddModelError(string.Empty, "The record you attempted to edit "
                    + "was modified by another user after you got the original value. The "
                    + "edit operation was canceled and the current values in the database "
                    + "have been displayed. If you still want to edit this record, click "
                    + "the Save button again. Otherwise click the Back to List hyperlink.");
                department.Timestamp = databaseValues.Timestamp;
            }*/

            return false;
        }
    
        protected ActionResult GoToIndex()
        {
            return RedirectToAction("Index");
        }
    }
}