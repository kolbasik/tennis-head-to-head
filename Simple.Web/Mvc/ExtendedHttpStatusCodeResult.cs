using System.Net;
using System.Web.Mvc;

namespace Simple.Web.Mvc
{
    public class ExtendedHttpStatusCodeResult : HttpStatusCodeResult
    {
        private readonly string _body;

        public ExtendedHttpStatusCodeResult(HttpStatusCode statusCode, string statusDescription)
            : this(statusCode, statusDescription, statusDescription)
        {
        }

        public ExtendedHttpStatusCodeResult(HttpStatusCode statusCode, string statusDescription, string body)
            : base((int)statusCode, statusDescription)
        {
            _body = body;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            base.ExecuteResult(context);
            var response = context.RequestContext.HttpContext.Response;
            response.Write(_body);
        }
    }
}