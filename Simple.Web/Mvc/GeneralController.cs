using System;
using System.Web.Mvc;
using Simple.Core.Data;

namespace Simple.Web.Mvc
{
    public abstract class GeneralController : Controller
    {
        protected override ITempDataProvider CreateTempDataProvider()
        {
            return new CookieTempDataProvider();
        }
    }

    public abstract class GeneralController<TContext> : GeneralController
        where TContext : IDataContext
    {
        protected readonly IUnitOfWork<TContext> UnitOfWork;

        protected GeneralController(IUnitOfWork<TContext> unitOfWork)
        {
            if (unitOfWork == null) throw new ArgumentNullException("unitOfWork");
            UnitOfWork = unitOfWork;
        }

        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            if (!filterContext.IsChildAction && (filterContext.Exception == null || filterContext.ExceptionHandled))
            {
                UnitOfWork.Commit();
            }
            base.OnActionExecuted(filterContext);
        }
    }
}