﻿using System;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using System.Linq;

namespace Simple.Web.Mvc
{
    /// <summary>
    /// Route Constraints: Working with conflicting routes in MVC
    ///     http://www.babel-lutefisk.net/2011/11/route-constraints-working-with.html
    /// </summary>
    public class ValuesConstraint : IRouteConstraint
    {
        private readonly bool _include;
        private readonly string[] _values;

        public ValuesConstraint(params string[] values) : this(true, values) { }
        public ValuesConstraint(bool include, params string[] values)
        {
            _include = include;
            _values = values;
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            return _include && (_values.Contains(values[parameterName].ToString(), StringComparer.InvariantCultureIgnoreCase));
        }

        public static string[] GetActionNames(params Type[] controllers)
        {
            var actionNames = from controllerType in controllers
                              from methodInfo in controllerType.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly)
                              select methodInfo.Name;
            return actionNames.ToArray();
        }
    }
}