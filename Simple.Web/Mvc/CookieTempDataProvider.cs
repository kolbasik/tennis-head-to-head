using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;
using System.Web.Mvc;

namespace Simple.Web.Mvc
{
    public class CookieTempDataProvider : ITempDataProvider
    {
        private const string TempDataCookieKey = "__Controller::TempData";

        IDictionary<string, object> ITempDataProvider.LoadTempData(ControllerContext controllerContext)
        {
            return LoadTempData(controllerContext);
        }

        void ITempDataProvider.SaveTempData(ControllerContext controllerContext, IDictionary<string, object> values)
        {
            SaveTempData(controllerContext, values);
        }

        protected virtual IDictionary<string, object> LoadTempData(ControllerContext controllerContext)
        {
            var cookie = controllerContext.HttpContext.Request.Cookies[TempDataCookieKey];
            var dictionary = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            if ((cookie == null) || String.IsNullOrEmpty(cookie.Value))
            {
                return dictionary;
            }
            foreach (var key in cookie.Values.AllKeys)
            {
                dictionary[key] = cookie[key];
            }
            cookie.Expires = DateTime.MinValue;
            cookie.Value = String.Empty;
            return dictionary;
        }

        protected virtual void SaveTempData(ControllerContext controllerContext, IDictionary<string, object> values)
        {
            if (values.Count <= 0) return;
            var cookie = new HttpCookie(TempDataCookieKey) { HttpOnly = true };
            foreach (var item in values)
            {
                cookie[item.Key] = Convert.ToString(item.Value, CultureInfo.InvariantCulture);
            }
            controllerContext.HttpContext.Response.Cookies.Add(cookie);
        }
    }
}