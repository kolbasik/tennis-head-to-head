﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Filters;
using Simple.Core.Data;

namespace Simple.Web.WebApi
{
    public class ExtendedQueryableAttribute : QueryableAttribute
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            var response = actionExecutedContext.Response;
            if (response == null || !response.IsSuccessStatusCode)
                return;

            var request = actionExecutedContext.Request;

            var objectContent = response.Content as ObjectContent;
            if (objectContent != null && request.RequestUri != null && !string.IsNullOrWhiteSpace(request.RequestUri.Query))
            {
                var dataSet = objectContent.Value as IDataSet;
                if (dataSet == null) return;
                try
                {
                    var queryable = PrepareDataSet(dataSet, request);
                    objectContent.Value = queryable;
                }
                catch (Exception ex)
                {
                    actionExecutedContext.Response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ex.Message);
                }
            }

            base.OnActionExecuted(actionExecutedContext);
        }

        private static IDataSet PrepareDataSet(IDataSet source, HttpRequestMessage request)
        {
            var urlQueryPath = request.RequestUri.ParseQueryString();
            var expands = urlQueryPath["$expand"];
            if (string.IsNullOrWhiteSpace(expands))
            {
                return source;
            }

            return expands.Split(',')
                .Select(s => s.Trim().Replace("/", "."))
                .Aggregate(source, (current, expand) => current.Include(expand));
        }

        public override void ValidateQuery(HttpRequestMessage request)
        {
            // don't report error for unsupported query
        }
    }
}