using System;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Simple.Core.Data;

namespace Simple.Web.WebApi
{
    public abstract class GeneralApiController<TContext, TEntity> : ApiController
        where TContext : IDataContext
        where TEntity : class, IDataEntity
    {
        protected readonly IDataRepository<TEntity> Repository;

        protected GeneralApiController(IUnitOfWork<TContext> unitOfWork)
        {
            if (unitOfWork == null) throw new ArgumentNullException("unitOfWork");
            Repository = unitOfWork.Context.GetRepositoryFor<TEntity>();
        }

        public IQueryable<TEntity> GetAll()
        {
            return Repository.GetAll();
        }

        public HttpResponseMessage Get(int id)
        {
            var model = Repository.GetById(id);
            if (model == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            return Request.CreateResponse(HttpStatusCode.OK, model);
        }

        public HttpResponseMessage Put(int id, TEntity model)
        {
            if (ModelState.IsValid && id == model.Id)
            {
                try
                {
                    Repository.Update(model);
                    Repository.Save();
                }
                catch (DataException)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);
                }

                return Request.CreateResponse(HttpStatusCode.OK);
            }
            
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        public HttpResponseMessage Post(TEntity model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Repository.Insert(model);
                    Repository.Save();
                }
                catch (DataException)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError);
                }

                var response = Request.CreateResponse(HttpStatusCode.Created, model);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = model.Id }));
                return response;
            }
            
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        public HttpResponseMessage Delete(int id)
        {
            TEntity model = Repository.GetById(id);
            if (model == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            try
            {
                Repository.Delete(model);
                Repository.Save();
            }
            catch (DataException)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError);
            }

            return Request.CreateResponse(HttpStatusCode.OK, model);
        }
    }
}